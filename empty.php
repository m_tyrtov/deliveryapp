<?php
require_once ($_SERVER['DOCUMENT_ROOT']."/includes/templates/main/header.php");
?>
<div class="container">
    <div class="row">
        <div class="col-sm-8" style="margin: auto;">
        <?php
            if (count($_GET['accept'])>0){
                $user = R::findOne('newusers', 'authkey = ?', array($_GET['accept']));
                if($user){
                    $auth_user = R::dispense('users');

                    $auth_user->name = $user->name;
                    $auth_user->email = $user->email;
                    $auth_user->login = $user->login;
                    $auth_user->password = $user->password;
                    $auth_user->order_count = 0;
                    $auth_user->order_rating = 0;

                    R::store($auth_user);
                    R::trash($user);
                }
            }
        ?>
        </div>
    </div>
</div>
<?php
require_once ($_SERVER['DOCUMENT_ROOT']."/includes/templates/main/footer.php");
?>
