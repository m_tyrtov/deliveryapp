<?php
require_once ($_SERVER['DOCUMENT_ROOT']."/includes/components/DBConnect/db.php");

class Order

{

    public function add($name, $item, $addsA, $addsB){
        $orders = R::dispense( 'orders' );
        $orders->id_user = $name;
        $orders->order_name = $item;
        $orders->pointA = $addsA;
        $orders->pointB = $addsB;
        $orders->status = "new";
        R::store($orders);
    }

    public function check(){
        $orders = R::findAll('orders', 'ORDER by `id` DESC');
        for($i = 1; $i<=count($orders); $i++){
            $resultOrders = array();
            if ($orders[$i]->status == "new"){
                $ORDER = $orders[$i];
                $resultOrders[] = $ORDER;
                $ORDER->status = "old";
                R::store($ORDER);
            }
        }
        return $resultOrders;
    }

}

?>