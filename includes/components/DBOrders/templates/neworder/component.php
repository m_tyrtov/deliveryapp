<?php
if ( isset ($_SESSION['logged_user']) ){
?>
<script src="/js/sendform/script.js" type="text/javascript"></script>
<h3 style="text-align: center;">Форма подачи заказа</h3>
<div id="send-form" class="form-group">
    <div class="form-group">
        <label for="name">Ваше имя:</label>
        <input type="text" class="form-control" id="name" value="<?php echo $_SESSION['logged_user']->name; ?>" disabled>
    </div>
    <div class="form-group">
        <label for="adds-a">Откуда забрать:</label><br>
        <button id="show-map-a" type="button" class="btn btn-primary btn-block">Показать карту</button><br>
        <div id="map-a" style="width: 100%; height: 400px; outline: 1px solid #ced4da; margin-bottom: 1rem; display: none" ></div>
        <input type="text" class="form-control" id="adds-a">
    </div>
    <div class="form-group">
        <label for="adds-b">Куда доставить:</label><br>
        <button id="show-map-b" type="button" class="btn btn-primary btn-block">Показать карту</button><br>
        <div id="map-b" style="width: 100%; height: 400px; outline: 1px solid #ced4da; margin-bottom: 1rem; display: none" ></div>
        <input type="text" class="form-control" id="adds-b">
    </div>
    <div class="form-group">
        <label for="item">Что забрать:</label>
        <input type="text" class="form-control" id="item">
    </div>
    <div>
        <h4 style="text-align: center;">Расчет стоимости</h4>
        <table class="table">
            <tbody>
            <tr>
                <td><b>Выезд курьера:<b></td>
                <td colspan="2" align="center">+ <span id="fastboy-money">150</span> р</td>
            </tr>
            <tr>
                <td><b>Расстояние:<b></td>
                <td><span id="table-km">0 км</span></td>
                <td>+ <span id="table-km-money">0</span> р</td>
            </tr>
            <tr>
                <td><b>Габариты:<b></td>
                <td colspan="2" align="center">+ 0 р</td>
            </tr>
            <tr>
                <td><b>Масса:<b></td>
                <td colspan="2" align="center">+ 0 р</td>
            </tr>
            <tr>
                <td><b>Трафик:<b></td>
                <td colspan="2" align="center">+ 0 р</td>
            </tr>
            <tr>
                <td><h5>Итого:</h5></td>
                <td colspan="2" align="center"><b><h5><span id="result-money">0</span> р</h5><b></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div style="display: none;" id="ok-alert" class="alert alert-success">
    <strong>Успешно!</strong> Ваш заказ передан в исполнение.
</div>
<div style="display: none;" id="fail-alert" class="alert alert-danger">
    <strong>Ошибка!</strong> Проверьте правильность заполнения полей.
</div>
<div style="text-align: center">
    <button id="send-order-btn" class="btn btn-success">Отправить заявку</button>
</div>
<?php
}else{
?>
<h3 style="text-align: center;">Необходимо авторизоваться</h3>
<div style="text-align: center">
    <h2><a href="/auth/login/">Авторизация</a></h2>
    <h2><a href="/auth/signup/">Регистрация</a></h2>
</div>
<?php
}
?>