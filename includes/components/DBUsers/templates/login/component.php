<?php
    require_once ($_SERVER['DOCUMENT_ROOT']."/includes/components/DBUsers/class.php");

    $data = $_POST;
    if (isset($data['do_login']))
    {
        Users::login($data);
    }
?>
<div class="jumbotron text-center">
    <h1>Авторизация аккаунта</h1>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-8" style="margin: auto;">
            <form action="index.php" method="POST">
                <h3 style="text-align: center;">Укажите свои данные</h3>
                <div id="send-form" class="form-group">
                    <div class="form-group">
                        <label for="login">Номер телефона</label>
                        <input type="text" name="login" value="<?php echo @$data['login']; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="password">Пароль:</label>
                        <input type="password" name="password" class="form-control">
                    </div>
                </div>
                <div style="text-align: center">
                    <button type="submit" name="do_login" class="btn btn-primary">Войти</button>
                </div>
            </form>
        </div>
    </div>
</div>