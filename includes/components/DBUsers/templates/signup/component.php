<?php
    require_once ($_SERVER['DOCUMENT_ROOT']."/includes/components/DBUsers/class.php");

    $data = $_POST;

    //если кликнули на button
    if (isset($data['do_signup']))
    {
        Users::signup($data);
    }
?>
<div class="jumbotron text-center">
    <h1>Регистрация аккаунта</h1>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-8" style="margin: auto;">
            <form action="/auth/signup/index.php" method="POST">
                <h3 style="text-align: center;">Укажите свои данные</h3>
                <div id="send-form" class="form-group">
                    <div class="form-group">
                        <label for="surname">Фамилия:</label>
                        <input type="text" name="surname" value="<?php echo @$data['surname']; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="name">Имя:</label>
                        <input type="text" name="name" value="<?php echo @$data['name']; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="patronymic">Отчество:</label>
                        <input type="text" name="patronymic" value="<?php echo @$data['patronymic']; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="adds-a">E-mail:</label>
                        <input type="email" name="email" value="<?php echo @$data['email']; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="adds-b">Номер телефона</label>
                        <input type="text" name="login" value="<?php echo @$data['login']; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="item">Пароль:</label>
                        <input type="password" name="password" value="<?php echo @$data['password']; ?>" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="item">Еще раз пароль:</label>
                        <input type="password" name="password_2" value="<?php echo @$data['password_2']; ?>" class="form-control">
                    </div>
                </div>
                <div style="text-align: center">
                    <button type="submit" name="do_signup" class="btn btn-success">Зарегистрироваться</button>
                </div>
            </form>
        </div>
    </div>
</div>