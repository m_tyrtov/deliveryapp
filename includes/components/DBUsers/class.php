<?php
/**
 * Created by PhpStorm.
 * User: kimonz
 * Date: 08/10/2018
 * Time: 23:57
 */

require_once ($_SERVER['DOCUMENT_ROOT']."/includes/components/DBConnect/db.php");

class Users

{
    public function signup($data){
        $errors = array();
        if (trim($data['surname']) == '')
        {
            $errors[] = 'Введите фамилию';
        }

        if (trim($data['name']) == '')
        {
            $errors[] = 'Введите имя';
        }

        if (trim($data['patronymic']) == '')
        {
            $errors[] = 'Введите отчество';
        }

        if (trim($data['email']) == '')
        {
            $errors[] = 'Введите Email';
        }

        if (trim($data['login']) == '')
        {
            $errors[] = 'Введите номер телефона';
        }

        if ($data['password'] == '')
        {
            $errors[] = 'Введите пароль';
        }

        if ($data['password_2'] != $data['password'])
        {
            $errors[] = 'Повторный пароль введен не верно!';
        }

        if (R::count('newusers', "login = ?", array($data['login'])) > 0)
        {
            $errors[] = 'Пользователь с таким логином уже существует!';
        }

        if (R::count('newusers', "email = ?", array($data['email'])) > 0)
        {
            $errors[] = 'Пользователь с таким Email уже существует!';
        }

        if (empty($errors)){
            $user = R::dispense('newusers');
            $user->name = $data['surname']." ".$data['name']." ".$data['patronymic'];
            $user->email = $data['email'];
            $user->login = $data['login'];
            $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
            $user->authkey = sha1(random_bytes(16));
            R::store($user);
            header('Location: /auth/login/');
        }else{
            echo '<div id="errors" style="color:red;">' .array_shift($errors). '</div><hr>';
        }


    }

    public function login($data){
        $user = R::findOne('newusers', 'login = ?', array($data['login']));
        if ($user){
            //логин существует
            if (password_verify($data['password'], $user->password)){
                //если пароль совпадает, то нужно авторизовать пользователя
                $_SESSION['logged_user'] = $user;
                header('Location: /');
            }else{
                $errors[] = 'Неверно введен пароль!';
            }
        }else{
            $errors[] = 'Пользователь с таким логином не найден!';
        }

        if (!empty($errors))
        {
            //выводим ошибки авторизации
            echo '<div id="errors" style="color:red;">' .array_shift($errors). '</div><hr>';
        }
    }
}

?>