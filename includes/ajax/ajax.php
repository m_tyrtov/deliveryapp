<?php
require_once ($_SERVER['DOCUMENT_ROOT']."/includes/components/DBAction/class.php");

if ($_POST['type'] == 'send'){ //если получен запрос на отправку

    Order::add($_POST['name'], $_POST['item'], $_POST['adds'], $_POST['addsB']); //вызываем функцию добавления заказа в таблицу

    echo json_encode(array(
        'result'    => 'success'
    ));
}

if ($_POST['type'] == 'check'){ //если получен запрос на проверку обновлений

    $html = Order::check();

    echo json_encode(array(
        'result'    => 'success',
        'html'      => $html
    ));
}
?>