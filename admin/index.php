<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script src="/js/acceptform/script.js"></script>
    </head>
<body>

<div class="jumbotron text-center">
  <h1><?php echo 'Сторона курьера';?></h1>
</div>
  
<div class="container">
	<div class="row">
		<div class="col-xl-auto" style="margin: auto;">
			<h3 style="text-align: center;">Новые заказы</h3>
			<br>
			<table class="table">
			    <thead>
			      <tr>
			        <th>Клиент</th>
			        <th>Товар</th>
			        <th>Откуда</th>
                    <th>Куда</th>
			      </tr>
			    </thead>
			    <tbody id="order-table">

			    </tbody>
		  </table>
		</div>
	</div>
</div>

</body>
</html>
