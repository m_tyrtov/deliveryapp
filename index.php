<?php
    require_once ($_SERVER['DOCUMENT_ROOT']."/includes/templates/main/header.php");
?>

<div class="jumbotron text-center">
  <h1>Заказ доставки</h1>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-8" style="margin: auto;">
            <?php
            require_once ($_SERVER['DOCUMENT_ROOT']."/includes/components/DBOrders/templates/neworder/component.php");
            ?>
		</div>
	</div>
</div>

<?php
    require_once ($_SERVER['DOCUMENT_ROOT']."/includes/templates/main/footer.php");
?>
