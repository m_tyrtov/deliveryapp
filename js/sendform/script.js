$( document ).ready(function() {

    ymaps.ready(init);

    function init() {
        arrCoords = ["", ""];

        //ФУНКЦИИ
        //узнаем расстояние между точками
        function lengthPoints(coord1, coord2){
            //ТЕСТ РАСЧЕТА РАССТОЯНИЯ
            referencePoints = [coord1, coord2];
            var multiRoute = new ymaps.multiRouter.MultiRoute({
                // Описание опорных точек мультимаршрута.
                referencePoints,
                params: {
                    // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                    results: 1
                }
            });

            multiRoute.model.events.add("requestsuccess", function (event) {
                if(multiRoute.getRoutes().get(0) != null){
                    if (multiRoute.getRoutes().get(0).properties.get('distance').value >= 1000){
                        lengthShow.innerText = multiRoute.getRoutes().get(0).properties.get('distance').text;
                    }else{
                        lengthShow.innerText = "0 км";
                    }
                    lengthMoneyShow.innerText = Math.round((multiRoute.getRoutes().get(0).properties.get('distance').value)/1000)*20;
                    moneyResult.innerText = parseInt(moneyBoy.innerText, 10)+parseInt(lengthMoneyShow.innerText, 10);
                }else{
                    console.log("Неверный адрес!");
                }
            });
        }

        //ИНТЕРФЕЙС
        //Поле с адресом А
        showGeo = document.getElementById('adds-a');
        showGeo.onchange = function () { //Изменение конечного адреса
            arrCoords[0] = this.value;
            lengthPoints(arrCoords[0], arrCoords[1]);
        };
        showGeo.disabled = true;
        showGeo.value = "Определяем адрес...";

        //Поле с адресом Б
        showGeoB = document.getElementById('adds-b');
        showGeoB.onchange = function () { //Изменение конечного адреса
            arrCoords[1] = this.value;
            lengthPoints(arrCoords[0], arrCoords[1]);
        };

        //Поле с КМ
        lengthShow = document.getElementById('table-km');
        //Поле с рассчетом цены за КМ
        lengthMoneyShow = document.getElementById('table-km-money');
        //Цена курьера
        moneyBoy = document.getElementById('fastboy-money');
        //Итоговая цена
        moneyResult = document.getElementById('result-money');

        //API YANDEX
        // Инциализируем карту А
        var geolocation = ymaps.geolocation,
            myPlacemark,
            myMap = new ymaps.Map('map-a', {
                center: [55, 34],
                zoom: 16
            }, {
                searchControlProvider: 'yandex#search'
            });
        // Инциализируем карту А
        var myPlacemarkB,
            myMapB = new ymaps.Map('map-b', {
                center: [55, 34],
                zoom: 16
            }, {
                searchControlProvider: 'yandex#search'
            });

        // Определяем геопозицию
        geolocation.get({
            provider: 'browser',
            mapStateAutoApply: false
        }).then(function (result) {
            // Получение местоположения пользователя.
            var userAddress = result.geoObjects.get(0).properties.get('text');
            var userCoodinates = result.geoObjects.get(0).geometry.getCoordinates();
            // Пропишем полученный адрес в балуне.
            result.geoObjects.get(0).properties.set({
                balloonContentBody: 'Адрес: ' + userAddress +
                '<br/>Координаты:' + userCoodinates
            });
            myMap.geoObjects.add(result.geoObjects);
            myMap.setCenter(userCoodinates);
            myMapB.setCenter(userCoodinates);

            showGeo.value = userAddress;
            showGeo.disabled = false;
            arrCoords[0] = userCoodinates;
            //console.log(userCoodinates); <---- координаты определенные по геопозиции
        });

        // Слушаем клик на карте A
        myMap.events.add('click', function (e) {
            var coords = e.get('coords');

            // Если метка уже создана – просто передвигаем ее.
            if (myPlacemark) {
                myPlacemark.geometry.setCoordinates(coords);
            }
            // Если нет – создаем.
            else {
                myPlacemark = createPlacemark(coords);
                myMap.geoObjects.add(myPlacemark);
                // Слушаем событие окончания перетаскивания на метке.
                myPlacemark.events.add('dragend', function () {
                    getAddress(myPlacemark.geometry.getCoordinates());
                });
            }
            getAddress(coords, myPlacemark, showGeo);
            arrCoords[0] = coords;
        });

        // Слушаем клик на карте Б
        myMapB.events.add('click', function (e) {
            var coords = e.get('coords');

            // Если метка уже создана – просто передвигаем ее.
            if (myPlacemarkB) {
                myPlacemarkB.geometry.setCoordinates(coords);
            }
            // Если нет – создаем.
            else {
                myPlacemarkB = createPlacemark(coords);
                myMapB.geoObjects.add(myPlacemarkB);
                // Слушаем событие окончания перетаскивания на метке.
                myPlacemarkB.events.add('dragend', function () {
                    getAddress(myPlacemarkB.geometry.getCoordinates());
                });
            }
            getAddress(coords, myPlacemarkB, showGeoB);
            arrCoords[1] = coords;
            lengthPoints(arrCoords[0], arrCoords[1]);
        });

        // Создание метки.
        function createPlacemark(coords) {
            return new ymaps.Placemark(coords, {
                iconCaption: 'поиск...'
            }, {
                preset: 'islands#violetDotIconWithCaption',
                draggable: true
            });
        }

        // Определяем адрес по координатам (обратное геокодирование).
        function getAddress(coords, placemark, geoinput) {
            placemark.properties.set('iconCaption', 'поиск...');
            ymaps.geocode(coords).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0);
                placemark.properties
                    .set({
                        // Формируем строку с данными об объекте.
                        iconCaption: [
                            // Название населенного пункта или вышестоящее административно-территориальное образование.
                            firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                            // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
                            firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
                        ].filter(Boolean).join(', '),
                        // В качестве контента балуна задаем строку с адресом объекта.
                        balloonContent: firstGeoObject.getAddressLine()
                    });
                geoinput.value = firstGeoObject.getAddressLine();
            });
        }

    }

    //форма
    sendform = document.getElementById('send-form');

    //поля
    input_name = document.getElementById('name');
    input_item = document.getElementById('item');
    input_adds = document.getElementById('adds-a');
    input_addsB = document.getElementById('adds-b');

    //алерты
    alert_ok = document.getElementById('ok-alert');
    alert_fail = document.getElementById('fail-alert');

    //кнопки
    aMapBtn = document.getElementById('show-map-a');
    aMapBtn.onclick = function(){
        if(aMap.style.display == "none"){
            aMap.style.display = "block";
            this.innerText = "Cкрыть карту";
        }else{
            aMap.style.display = "none";
            this.innerText = "Показать карту";
        }
    };

    bMapBtn = document.getElementById('show-map-b');
    bMapBtn.onclick = function(){
        if(bMap.style.display == "none"){
            bMap.style.display = "block";
            this.innerText = "Cкрыть карту";
        }else{
            bMap.style.display = "none";
            this.innerText = "Показать карту";
        }
    };

    //карты
    aMap = document.getElementById('map-a');
    bMap = document.getElementById('map-b');

    //ВИЗУАЛИЗАЦИЯ
    //успешная отправка
    function okForm(){
        sendform.style.display = "none"; //скрываем форму
        sendbtn.style.display = "none"; //скрываем кнопку
        alert_fail.style.display = "none"; //скрываем уведомление о неудаче
        alert_ok.style.display = "block"; //отображаем уведомление
    }

    //требование поправить форму (отправки не будет)
    function failForm(){
        alert_fail.style.display = "block"; //отображаем уведомление о неудаче
    }

    // ФУНКЦИОНАЛ
    // кнопка отправки
    sendbtn = document.getElementById('send-order-btn');
    sendbtn.onclick = function(){
        if((input_name.value != "") && (input_item.value != "") && (input_adds.value != "") && (input_addsB.value != "") && (input_adds.value != "Определяем адрес...")){
            $.ajax({ //подключаем ajax
                url: "/includes/ajax/ajax.php", //путь
                type: "post", //метод передачи данных
                dataType: "json", //тип передачи данных
                data: {
                    "type":   "send",
                    "name":   input_name.value,
                    "item":   input_item.value,
                    "adds":   input_adds.value,
                    "addsB":   input_addsB.value
                },
                success: function(data){
                    if(data.result == "success"){
                        okForm();
                    }
                }
            });
        }else{
            failForm();
        }
    };

});
