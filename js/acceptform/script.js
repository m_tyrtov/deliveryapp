$( document ).ready(function() {

    orderTable = document.getElementById('order-table');

    //ВУЗУАЛИЗАЦИЯ
    //добавить в таблицу новые заказы
    function showOrder(name, item, pointA, pointB) {
        newname = document.createElement('td');
            newname.innerText = name;
        newitem = document.createElement('td');
            newitem.innerText = item;
        newpointA = document.createElement('td');
            newpointA.innerText = pointA;
        newpointB= document.createElement('td');
            newpointB.innerText = pointB;
        newOrder = document.createElement('tr');

        newOrder.appendChild(newname);
        newOrder.appendChild(newitem);
        newOrder.appendChild(newpointA);
        newOrder.appendChild(newpointB);

        orderTable.appendChild(newOrder);
    }

    //ФУНКЦИОНАЛ
    //проверка БД на обновление
    function acceptOrder() {
        $.ajax({ //подключаем ajax
            url: "/includes/ajax/ajax.php", //путь
            type: "post", //метод передачи данных
            dataType: "json", //тип передачи данных
            data: {
                "type":   "check"
            },
            success: function(data){
                if((data.result == "success") && (data.html != null)){
                    for(i = 0; i<data.html.length; i++){
                        showOrder(data.html[i]['id_user'], data.html[i]['order_name'], data.html[i]['point_a'], data.html[i]['point_b']);
                        console.log(data.html);
                    }
                }
            }
        });
    }

   requestServer = setInterval(acceptOrder, 3000);

});